//
//  IntExtensionTests.swift
//  EmpatKaliTestTests
//
//  Created by Rian Erlangga Saputra on 10/08/20.
//  Copyright © 2020 rerlanggas. All rights reserved.
//

import XCTest
@testable import EmpatKaliTest

class IntExtensionTests: XCTestCase {

    override func setUpWithError() throws {
        
    }

    override func tearDownWithError() throws {
        
    }

    func testformattedWithSeparatorWithRp() {
        let value: Int = 100000
        XCTAssertEqual(value.formattedWithSeparatorWithRp, "Rp. 100.000")
    }

    func testformattedWithSeparator() {
        let value: Int = 100000
        XCTAssertEqual(value.formattedWithSeparator, "100.000")
    }

}
