//
//  HomeViewModelTests.swift
//  EmpatKaliTestTests
//
//  Created by Rian Erlangga Saputra on 10/08/20.
//  Copyright © 2020 rerlanggas. All rights reserved.
//

import XCTest
@testable import EmpatKaliTest

class HomeViewModelTests: XCTestCase {
    
    var sut: HomeViewModel!
    
    override func setUpWithError() throws {
        sut = HomeViewModel()
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testLoadBanners() {
        let e = expectation(description: "Load Banners API")
        sut.error = { error in
            XCTAssertNil(error, error)
            e.fulfill()
        }
        sut.bannersReceived = {
            XCTAssert(self.sut.homeMenus.count > 0, "HomeMenus is empty")
            XCTAssert(self.sut.banners.count > 0, "Banners is empty")
            e.fulfill()
        }
        sut.loadBanners()
        
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func testLoadShops() {
        let e = expectation(description: "Load Shops API")
        sut.error = { error in
            XCTAssertNil(error, error)
            e.fulfill()
        }
        sut.shopsReceived = {
            XCTAssert(self.sut.homeMenus.count > 0, "HomeMenus is empty")
            XCTAssert(self.sut.shops.count > 0, "Shops is empty")
            e.fulfill()
        }
        sut.loadShops()
        
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func testAssignHomeMenus() {
        sut.assignHomeMenus()
        XCTAssert(self.sut.homeMenus.count > 0, "HomeMenus is empty")
    }

}
