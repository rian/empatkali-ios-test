//
//  ProfileViewModelTests.swift
//  EmpatKaliTestTests
//
//  Created by Rian Erlangga Saputra on 10/08/20.
//  Copyright © 2020 rerlanggas. All rights reserved.
//

import XCTest
@testable import EmpatKaliTest

class ProfileViewModelTests: XCTestCase {
    
    var sut: ProfileViewModel!
    
    override func setUpWithError() throws {
        sut = ProfileViewModel()
    }
    
    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testLoadProfile() {
        let e = expectation(description: "Load Profile API")
        sut.error = { error in
            XCTAssertNil(error, error)
            e.fulfill()
        }
        sut.dataReceived = {
            XCTAssertNotNil(self.sut.data, "Data Profile is empty")
            e.fulfill()
        }
        sut.loadProfile()
        
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
}
