//
//  WebViewModelTests.swift
//  EmpatKaliTestTests
//
//  Created by Rian Erlangga Saputra on 10/08/20.
//  Copyright © 2020 rerlanggas. All rights reserved.
//

import XCTest
@testable import EmpatKaliTest

class WebViewModelTests: XCTestCase {
    
    var sut: WebViewModel!
    
    override func setUpWithError() throws {
        sut = WebViewModel()
    }
    
    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testisValidUrl() {
        XCTAssertTrue(sut.isValidUrl(urlString: "http://google.com"), "URL is invalid")
    }
    
}
