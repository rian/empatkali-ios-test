//
//  BaseViewModel.swift
//  EmpatKaliTest
//
//  Created by Rian Erlangga Saputra on 10/08/20.
//  Copyright © 2020 rerlanggas. All rights reserved.
//

import Foundation

import Foundation

class BaseViewModel {
    var showLoading: (_ loading: Bool) -> Void = { loading in
        DispatchQueue.main.async {
            if loading {
                ProgressHUD.show("Please Wait".localized)
            } else {
                ProgressHUD.dismiss()
            }
        }
    }
    var dataReceived: () -> Void = {}
    var error: (_ error: String) -> Void = { _ in}
}
