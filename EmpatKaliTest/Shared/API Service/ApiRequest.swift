//
//  ApiRequest.swift
//  EmpatKaliTest
//
//  Created by Rian Erlangga Saputra on 10/08/20.
//  Copyright © 2020 rerlanggas. All rights reserved.
//

import Foundation

import Alamofire

let baseUrl = "https://private-546c9-test5718.apiary-mock.com/"

class APIRequest<T: Decodable>: NSObject {
    func execute(url: String,
                 method: HTTPMethod,
                 parameters: Parameters,
                 success:@escaping (_ response: T) -> Void,
                 failure:@escaping (_ error: String?) -> Void) {
        
        if !NetworkReachabilityManager()!.isReachable {
            failure("Tidak ada koneksi internet")
            return
        }
        
        let headers: HTTPHeaders = [:]
        print("parameter: \(String(describing: parameters))")
        let dataRequest: DataRequest =  Alamofire.request(url,
                                                          method: method,
                                                          parameters: parameters,
                                                          encoding: URLEncoding.default,
                                                          headers: headers).debugLog()
        
        dataRequest.responseString() { response in
            #if DEBUG
            debugPrint(response)
            #endif
            switch response.result {
            case .success( _):
                do {
                    let apiResponse = try JSONDecoder().decode(T.self, from: response.data!)
                    DispatchQueue.main.async {
                        success(apiResponse)
                    }
                } catch {
                    print(error)
                    failure(NetworkResponse.unableToDecode.rawValue.localized)
                }
            case .failure(let error):
                failure(error.localizedDescription)
            }
        }
    }
    
}

extension Request {
    public func debugLog() -> Self {
        #if DEBUG
        debugPrint(self)
        #endif
        return self
    }
}

enum NetworkResponse: String {
    case noData = "Failed to get data."
    case unableToDecode = "Failed decode data"
}
