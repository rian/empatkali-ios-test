//
//  Banner.swift
//  EmpatKaliTest
//
//  Created by Rian Erlangga Saputra on 10/08/20.
//  Copyright © 2020 rerlanggas. All rights reserved.
//

import Foundation

struct Banner : Codable {
    let _id : String?
    let img : String?
    let detail : String?
    let url : String?
    let merchant : Merchant?
    let tags : [String]?
    let stores : [String]?
    let discount : Int?
    let active : Bool?
    let period : Period?
    let title : String?

    enum CodingKeys: String, CodingKey {

        case _id = "_id"
        case img = "img"
        case detail = "detail"
        case url = "url"
        case merchant = "merchant"
        case tags = "tags"
        case stores = "stores"
        case discount = "discount"
        case active = "active"
        case period = "period"
        case title = "title"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        _id = try values.decodeIfPresent(String.self, forKey: ._id)
        img = try values.decodeIfPresent(String.self, forKey: .img)
        detail = try values.decodeIfPresent(String.self, forKey: .detail)
        url = try values.decodeIfPresent(String.self, forKey: .url)
        merchant = try values.decodeIfPresent(Merchant.self, forKey: .merchant)
        tags = try values.decodeIfPresent([String].self, forKey: .tags)
        stores = try values.decodeIfPresent([String].self, forKey: .stores)
        discount = try values.decodeIfPresent(Int.self, forKey: .discount)
        active = try values.decodeIfPresent(Bool.self, forKey: .active)
        period = try values.decodeIfPresent(Period.self, forKey: .period)
        title = try values.decodeIfPresent(String.self, forKey: .title)
    }

}

struct Merchant : Codable {
    let _id : String?
    let image : String?
    let name : String?

    enum CodingKeys: String, CodingKey {

        case _id = "_id"
        case image = "image"
        case name = "name"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        _id = try values.decodeIfPresent(String.self, forKey: ._id)
        image = try values.decodeIfPresent(String.self, forKey: .image)
        name = try values.decodeIfPresent(String.self, forKey: .name)
    }

}

struct Period : Codable {
    let end : String?
    let start : String?

    enum CodingKeys: String, CodingKey {

        case end = "end"
        case start = "start"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        end = try values.decodeIfPresent(String.self, forKey: .end)
        start = try values.decodeIfPresent(String.self, forKey: .start)
    }

}
