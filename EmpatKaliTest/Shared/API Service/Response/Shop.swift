//
//  Shop.swift
//  EmpatKaliTest
//
//  Created by Rian Erlangga Saputra on 10/08/20.
//  Copyright © 2020 rerlanggas. All rights reserved.
//

import Foundation

struct Shop : Codable {
    let img : String?
    let detail : String?
    let url : String?
    let merchant : Merchant?
    let active : Bool?
    let period : String?

    enum CodingKeys: String, CodingKey {

        case img = "img"
        case detail = "detail"
        case url = "url"
        case merchant = "merchant"
        case active = "active"
        case period = "period"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        img = try values.decodeIfPresent(String.self, forKey: .img)
        detail = try values.decodeIfPresent(String.self, forKey: .detail)
        url = try values.decodeIfPresent(String.self, forKey: .url)
        merchant = try values.decodeIfPresent(Merchant.self, forKey: .merchant)
        active = try values.decodeIfPresent(Bool.self, forKey: .active)
        period = try values.decodeIfPresent(String.self, forKey: .period)
    }

}
