//
//  ProfileResponse.swift
//  EmpatKaliTest
//
//  Created by Rian Erlangga Saputra on 10/08/20.
//  Copyright © 2020 rerlanggas. All rights reserved.
//

import Foundation

struct ProfileResponse : Codable {
    let _id : String?
    let mobileNumber : String?
    let referralCode : String?
    let selfie : String?
    let defaultPayment : String?
    let registrationLoc : RegistrationLoc?
    let danaVerifiedAccount : Bool?
    let loc : Loc?
    let emailVerified : Bool?
    let card : [String]?
    let emergencyContact : EmergencyContact?
    let detail : Detail?
    let status : Int?
    let remainingCredit : Int?
    let activated : Bool?
    let ktp : Ktp?
    let credit : Int?

    enum CodingKeys: String, CodingKey {

        case _id = "_id"
        case mobileNumber = "mobileNumber"
        case referralCode = "referralCode"
        case selfie = "selfie"
        case defaultPayment = "defaultPayment"
        case registrationLoc = "registrationLoc"
        case danaVerifiedAccount = "danaVerifiedAccount"
        case loc = "loc"
        case emailVerified = "emailVerified"
        case card = "card"
        case emergencyContact = "emergencyContact"
        case detail = "detail"
        case status = "status"
        case remainingCredit = "remainingCredit"
        case activated = "activated"
        case ktp = "ktp"
        case credit = "credit"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        _id = try values.decodeIfPresent(String.self, forKey: ._id)
        mobileNumber = try values.decodeIfPresent(String.self, forKey: .mobileNumber)
        referralCode = try values.decodeIfPresent(String.self, forKey: .referralCode)
        selfie = try values.decodeIfPresent(String.self, forKey: .selfie)
        defaultPayment = try values.decodeIfPresent(String.self, forKey: .defaultPayment)
        registrationLoc = try values.decodeIfPresent(RegistrationLoc.self, forKey: .registrationLoc)
        danaVerifiedAccount = try values.decodeIfPresent(Bool.self, forKey: .danaVerifiedAccount)
        loc = try values.decodeIfPresent(Loc.self, forKey: .loc)
        emailVerified = try values.decodeIfPresent(Bool.self, forKey: .emailVerified)
        card = try values.decodeIfPresent([String].self, forKey: .card)
        emergencyContact = try values.decodeIfPresent(EmergencyContact.self, forKey: .emergencyContact)
        detail = try values.decodeIfPresent(Detail.self, forKey: .detail)
        status = try values.decodeIfPresent(Int.self, forKey: .status)
        remainingCredit = try values.decodeIfPresent(Int.self, forKey: .remainingCredit)
        activated = try values.decodeIfPresent(Bool.self, forKey: .activated)
        ktp = try values.decodeIfPresent(Ktp.self, forKey: .ktp)
        credit = try values.decodeIfPresent(Int.self, forKey: .credit)
    }

}
