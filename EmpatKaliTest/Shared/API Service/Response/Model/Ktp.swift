//
//  ProfileResponse.swift
//  EmpatKaliTest
//
//  Created by Rian Erlangga Saputra on 10/08/20.
//  Copyright © 2020 rerlanggas. All rights reserved.
//

import Foundation
struct Ktp : Codable {
	let number : String?
	let image : String?

	enum CodingKeys: String, CodingKey {

		case number = "number"
		case image = "image"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		number = try values.decodeIfPresent(String.self, forKey: .number)
		image = try values.decodeIfPresent(String.self, forKey: .image)
	}

}
