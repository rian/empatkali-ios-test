//
//  ProfileResponse.swift
//  EmpatKaliTest
//
//  Created by Rian Erlangga Saputra on 10/08/20.
//  Copyright © 2020 rerlanggas. All rights reserved.
//

import Foundation
struct EmergencyContact : Codable {
	let mobileNumber : String?
	let name : String?
	let type : String?

	enum CodingKeys: String, CodingKey {

		case mobileNumber = "mobileNumber"
		case name = "name"
		case type = "type"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		mobileNumber = try values.decodeIfPresent(String.self, forKey: .mobileNumber)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		type = try values.decodeIfPresent(String.self, forKey: .type)
	}

}
