//
//  ProfileResponse.swift
//  EmpatKaliTest
//
//  Created by Rian Erlangga Saputra on 10/08/20.
//  Copyright © 2020 rerlanggas. All rights reserved.
//

import Foundation
struct Detail : Codable {
	let gender : String?
	let email : String?
	let birthdate : String?
	let birthplace : String?
	let name : String?
	let penghasilan : String?
	let pendidikan : String?
	let pekerjaan : String?
	let industri : String?

	enum CodingKeys: String, CodingKey {

		case gender = "gender"
		case email = "email"
		case birthdate = "birthdate"
		case birthplace = "birthplace"
		case name = "name"
		case penghasilan = "penghasilan"
		case pendidikan = "pendidikan"
		case pekerjaan = "pekerjaan"
		case industri = "industri"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		gender = try values.decodeIfPresent(String.self, forKey: .gender)
		email = try values.decodeIfPresent(String.self, forKey: .email)
		birthdate = try values.decodeIfPresent(String.self, forKey: .birthdate)
		birthplace = try values.decodeIfPresent(String.self, forKey: .birthplace)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		penghasilan = try values.decodeIfPresent(String.self, forKey: .penghasilan)
		pendidikan = try values.decodeIfPresent(String.self, forKey: .pendidikan)
		pekerjaan = try values.decodeIfPresent(String.self, forKey: .pekerjaan)
		industri = try values.decodeIfPresent(String.self, forKey: .industri)
	}

}
