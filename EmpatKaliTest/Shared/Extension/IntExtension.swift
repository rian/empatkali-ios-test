//
//  IntExtension.swift
//  EmpatKaliTest
//
//  Created by Rian Erlangga Saputra on 10/08/20.
//  Copyright © 2020 rerlanggas. All rights reserved.
//

import Foundation

extension Formatter {
    static let withSeparator: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.groupingSeparator = "."
        formatter.numberStyle = .decimal
        return formatter
    }()
}

extension Int {
    var formattedWithSeparatorWithRp: String {
        return "Rp. \(Formatter.withSeparator.string(for: self) ?? "")"
    }
    var formattedWithSeparator: String {
        return "\(Formatter.withSeparator.string(for: self) ?? "")"
    }
}

extension Double {
    var formattedWithSeparatorWithRp: String {
        return "Rp. \(Formatter.withSeparator.string(for: self) ?? "")"
    }
    var formattedWithSeparator: String {
        return "\(Formatter.withSeparator.string(for: self) ?? "")"
    }
}
