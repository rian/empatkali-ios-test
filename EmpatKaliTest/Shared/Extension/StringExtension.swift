//
//  StringExtension.swift
//  EmpatKaliTest
//
//  Created by Rian Erlangga Saputra on 10/08/20.
//  Copyright © 2020 rerlanggas. All rights reserved.
//

import Foundation

extension String {
    
    // MARK: - Variable localized
    
    func localizeWithFormat(arguments: CVarArg...) -> String{
        return String(format: self.localized, arguments: arguments)
    }
    
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}
