//
//  ImageViewExtension.swift
//  EmpatKaliTest
//
//  Created by Rian Erlangga Saputra on 10/08/20.
//  Copyright © 2020 rerlanggas. All rights reserved.
//

import Foundation
import SDWebImage

extension UIImageView {
    func downloadImage(url: String, placeholder: UIImage = UIImage(named: "placeholder")!) {
        self.sd_setImage(with: URL(string: url), placeholderImage: placeholder)
    }
}
