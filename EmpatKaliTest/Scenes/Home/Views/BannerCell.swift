//
//  BannerCell.swift
//  EmpatKaliTest
//
//  Created by Rian Erlangga Saputra on 10/08/20.
//  Copyright © 2020 rerlanggas. All rights reserved.
//

import UIKit

class BannerCell: UITableViewCell {
    @IBOutlet weak var collectionView: UICollectionView!
    
    var banners: [Banner] = []
    var delegate: HomeDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setupCell(data: [Banner], delegate: HomeDelegate) {
        self.delegate = delegate
        self.banners = data
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: "HomeItemCell", bundle: nil), forCellWithReuseIdentifier: "HomeItemCell")
        collectionView.reloadData()
    }
    
}

extension BannerCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return banners.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeItemCell", for: indexPath) as! HomeItemCell
        cell.homeImageView.downloadImage(url: banners[indexPath.item].img ?? baseUrl)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let data = banners[indexPath.item]
        delegate?.itemTapped(url: data.url ?? "", title: data.title ?? "")
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = ((collectionView.frame.width - 40))
        return CGSize(width: width, height: 130)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
}
