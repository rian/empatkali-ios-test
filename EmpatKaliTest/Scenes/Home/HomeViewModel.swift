//
//  HomeViewModel.swift
//  EmpatKaliTest
//
//  Created by Rian Erlangga Saputra on 10/08/20.
//  Copyright © 2020 rerlanggas. All rights reserved.
//

import Foundation

class HomeViewModel: BaseViewModel {
    var banners: [Banner] = []
    var shops: [Shop] = []
    var homeMenus: [String] = []
    var bannersReceived: () -> Void = {}
    var shopsReceived: () -> Void = {}
    
    func loadBanners() {
        showLoading(true)
        APIRequest<[Banner]>().execute(url: baseUrl + "banners",
                                       method: .get,
                                       parameters: [:],
                                       success: {[weak self] response in
                                        self?.showLoading(false)
                                        self?.banners = response
                                        self?.assignHomeMenus()
                                        self?.bannersReceived()
            },
                                       failure: {[weak self] error in
                                        self?.showLoading(false)
                                        self?.error(error ?? "")
        })
    }
    
    func loadShops() {
        showLoading(true)
        APIRequest<[Shop]>().execute(url: baseUrl + "shops",
                                       method: .get,
                                       parameters: [:],
                                       success: {[weak self] response in
                                        self?.showLoading(false)
                                        self?.shops = response
                                        self?.assignHomeMenus()
                                        self?.shopsReceived()
            },
                                       failure: {[weak self] error in
                                        self?.showLoading(false)
                                        self?.error(error ?? "")
        })
    }
    
    func assignHomeMenus() {
        homeMenus = ["Banner","Man".localized,"Woman".localized,"Accessories".localized,"Zilingo".localized,"Medical Check Up".localized,"Coming Soon".localized,"Hot Deals".localized]
    }
}
