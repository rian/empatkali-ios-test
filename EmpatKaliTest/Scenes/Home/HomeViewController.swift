//
//  HomeViewController.swift
//  EmpatKaliTest
//
//  Created by Rian Erlangga Saputra on 10/08/20.
//  Copyright © 2020 rerlanggas. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    let viewModel = HomeViewModel()
    var refresher : UIRefreshControl?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupViewModel()
        setupTableView()
    }
    
    private func setupTableView() {
        
        refresher = UIRefreshControl()
        refresher?.addTarget(self, action: #selector(loadData), for: .valueChanged)
        tableView.addSubview(refresher!)
    }
    
    private func setupViewModel() {
        viewModel.error = {[weak self] error in
            self?.showAlertError(message: error)
        }
        viewModel.bannersReceived = {[weak self] in
            self?.refresher?.endRefreshing()
            self?.tableView.reloadData()
        }
        viewModel.shopsReceived = {[weak self] in
            self?.refresher?.endRefreshing()
            self?.tableView.reloadData()
        }
        loadData()
    }
    
    @objc private func loadData() {
        viewModel.loadBanners()
        viewModel.loadShops()
    }
}


extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.homeMenus.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 170
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "BannerCell") as! BannerCell
            cell.setupCell(data: viewModel.banners, delegate: self)
            cell.selectionStyle = .none
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ShopCell") as! ShopCell
            cell.setupCell(data: viewModel.shops, title: viewModel.homeMenus[indexPath.row], delegate: self)
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}

extension HomeViewController: HomeDelegate {
    func itemTapped(url: String, title: String) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
        vc.linkUrl = url
        vc.titleString = title
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

protocol HomeDelegate {
    func itemTapped(url: String, title: String)
}
