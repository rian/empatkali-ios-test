//
//  ProfileViewModel.swift
//  EmpatKaliTest
//
//  Created by Rian Erlangga Saputra on 10/08/20.
//  Copyright © 2020 rerlanggas. All rights reserved.
//

import Foundation
import AVFoundation

class ProfileViewModel: BaseViewModel {
    var data: ProfileResponse?
    
    func loadProfile() {
        showLoading(true)
        APIRequest<ProfileResponse>().execute(url: baseUrl + "profile",
                                       method: .get,
                                       parameters: [:],
                                       success: {[weak self] response in
                                        self?.showLoading(false)
                                        self?.data = response
                                        self?.dataReceived()
            },
                                       failure: {[weak self] error in
                                        self?.showLoading(false)
                                        self?.error(error ?? "")
        })
    }
    
    func isCameraAvailable() -> Bool {
        return UIImagePickerController.isSourceTypeAvailable(.camera)
    }
    
    func checkPermissionGranted(success:@escaping () -> Void,
                                failure:@escaping (_ error: String?) -> Void) {
        let authStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)

        if authStatus == AVAuthorizationStatus.denied {
            failure("Unable to access the Camera")

        } else if authStatus == AVAuthorizationStatus.notDetermined {
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (grantd) in
               if grantd {
                    success()
               } else {
                    failure("Need permission for camera")
                }
            })
        } else {
            success()
        }
    }
}
