//
//  ProfileViewController.swift
//  EmpatKaliTest
//
//  Created by Rian Erlangga Saputra on 10/08/20.
//  Copyright © 2020 rerlanggas. All rights reserved.
//

import UIKit

class ProfileViewController: UITableViewController {
    @IBOutlet weak var remainingCreditLabel: UILabel!
    @IBOutlet weak var creditLabel: UILabel!
    @IBOutlet weak var mobileNumberLabel: UILabel!
    @IBOutlet weak var referralLabel: UILabel!
    @IBOutlet weak var defaultPaymentLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var birthdateLabel: UILabel!
    @IBOutlet weak var birthplaceLabel: UILabel!
    @IBOutlet weak var incomeLabel: UILabel!
    @IBOutlet weak var educationLabel: UILabel!
    @IBOutlet weak var jobLabel: UILabel!
    @IBOutlet weak var industrionLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    
    let viewModel = ProfileViewModel()
    var refresher : UIRefreshControl?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViewModel()
        setupTableView()
    }
    
    private func setupTableView() {
        
        refresher = UIRefreshControl()
        refresher?.addTarget(self, action: #selector(loadData), for: .valueChanged)
        tableView.addSubview(refresher!)
    }
    
    private func setupViewModel() {
        viewModel.error = {[weak self] error in
            self?.showAlertError(message: error)
        }
        viewModel.dataReceived = {[weak self] in
            self?.initData()
        }
        loadData()
    }
    
    @objc private func loadData() {
        viewModel.loadProfile()
    }
    
    func initData() {
        refresher?.endRefreshing()
        if let data = viewModel.data {
            remainingCreditLabel.text = data.remainingCredit?.formattedWithSeparatorWithRp
            creditLabel.text = "/ \(data.credit?.formattedWithSeparatorWithRp ?? "")"
            mobileNumberLabel.text = data.mobileNumber
            referralLabel.text = data.referralCode
            defaultPaymentLabel.text = data.defaultPayment
            emailLabel.text = data.detail?.email
            birthdateLabel.text = data.detail?.birthdate
            birthplaceLabel.text = data.detail?.birthplace
            incomeLabel.text = data.detail?.penghasilan
            educationLabel.text = data.detail?.pendidikan
            jobLabel.text = data.detail?.pekerjaan
            industrionLabel.text = data.detail?.industri
            usernameLabel.text = data.detail?.name
            profileImageView.downloadImage(url: data.selfie ?? baseUrl, placeholder: UIImage())
        }
    }
    
    @IBAction func profileTapped(_ sender: Any) {
        showPictureActionSheet()
    }
    
    func showCamera() {
        if viewModel.isCameraAvailable() {
            viewModel.checkPermissionGranted(success: {[weak self] in
                self?.showImagePicker(.camera)
            }, failure: {[weak self] error in
                self?.showAlert(message: error ?? "")
            })
        }
    }
    
    private func showImagePicker(_ sourceType: UIImagePickerController.SourceType) {
        let imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = sourceType
        imagePicker.allowsEditing = true
        imagePicker.modalPresentationStyle = .popover
        present(imagePicker, animated: true, completion: nil)
        imagePicker.popoverPresentationController?.sourceView = view
        
    }
    
    private func showPictureActionSheet() {
        let actionSheetController = UIAlertController.init(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let takePhotoAction = UIAlertAction(title: "Take Photo".localized,
                                            style: .default,
                                            handler: {[weak self](UIAlertAction) in
                                                self?.showCamera()
                                                
        })
        
        let choosePhotoAction = UIAlertAction(title: "Choose From Gallery".localized,
                                              style: .default,
                                              handler: {[weak self](UIAlertAction) in
                                                self?.showImagePicker(.photoLibrary)
        })
        
        let cancelAction = UIAlertAction(title: "Cancel".localized, style: .cancel, handler: nil)
        
        actionSheetController.addAction(takePhotoAction)
        actionSheetController.addAction(choosePhotoAction)
        actionSheetController.addAction(cancelAction)
        actionSheetController.popoverPresentationController?.sourceView = view
        actionSheetController.popoverPresentationController?.sourceRect = CGRect(x: 0.0, y: 0.0, width: 1.0, height: 1.0)
        present(actionSheetController, animated: true, completion: nil)
    }
}

// MARK: Extension +UIImagePicker

extension ProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        dismiss(animated: true, completion: nil)
        if let pickedImage = info[.originalImage] as? UIImage {
            profileImageView.image = pickedImage
        }
    }
}
